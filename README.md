#Assignment

1) What assumptions did you make?
2) How does your algorithm work?
3) Do you think the (*hint) that we gave you earlier is correct and if so - why?
4) How precise do you think your outcome is and why?

## Assumptions
Amazon uses both prefix and suffix parameters, as well as groups keywords by their alias (mobile phones, etc).
To obtain accurate score we should obtain available aliases as well as keyword count for each alias.

## How does your algorithm work?
Sadly, it's a primitive algorithm that takes percentage of found keywords from max number (10).
Implementing complex will take much more time than 1 hour.

## Do you think the (*hint) that we gave you earlier is correct and if so - why
Provided data doesnt give me an ability to judge the correctness of this hint because it depends on a keyword.
Sigma (difference) between each found keyword will vary depending on this keyword score. The higher the score,
the less difference we will see in our array.

## How precise do you think your outcome is and why?
This is not precise at all and it looks like fortune telling.

# HOW-TO
To run this application simply use gradle wrapper: `./gradlew (or ./gradlew.bat for Windows) bootRun`