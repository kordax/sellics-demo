package com.sellics.demo.model;

public class Score {
    private final Integer score;
    private final String keyword;

    public Score(Integer score, String keyword) {
        this.score = score;
        this.keyword = keyword;
    }

    public Integer getScore() {
        return score;
    }

    public String getKeyword() {
        return keyword;
    }

    @Override
    public String toString() {
        return "Score{" +
                "score=" + score +
                ", keyword='" + keyword + '\'' +
                '}';
    }
}
