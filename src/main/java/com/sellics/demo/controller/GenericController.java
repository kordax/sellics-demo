package com.sellics.demo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.sellics.demo.client.AmazonCompleteClient;
import com.sellics.demo.model.AmazonResponse;
import com.sellics.demo.model.Score;
import com.weddini.throttling.Throttling;
import com.weddini.throttling.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.sellics.demo.utils.PrimitiveRankingSystem.rank;

/**
 * Generic REST controller for GET method that invokes.
 * Amazon AJAX reacts on each character used:
 * https://completion.amazon.com/api/2017/suggestions?session-id=132-8593035-8726248&customer-id=&request-id=W70V9B513AF5W7C9XCR6&page-type=Gateway&lop=en_US&site-variant=desktop&client-info=amazon-search-ui&mid=ATVPDKIKX0DER&alias=aps&suggestion-type=keyword&b2b=0&fresh=0&ks=undefined&prefix=&event=onFocusWithSearchTerm&limit=11&fb=1&_=1551352593578
 *
 */
@RestController
public class GenericController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericController.class);
    private final AmazonCompleteClient client;

    @Autowired
    GenericController(AmazonCompleteClient client) {
        this.client = client;
    }

    @GetMapping("/estimate")
    @Throttling(type = ThrottlingType.RemoteAddr, limit = 10)
    public ResponseEntity<Score> greeting(@RequestParam(value="keyword") String keyword) throws IOException {
        LOGGER.info("Handling keyword request for keyword: {}", keyword);
        final ResponseEntity<String> result = client.getScoreForKeyword(keyword);

        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enableDefaultTyping();

        ArrayNode amazonResponse = mapper.readValue(result.getBody(), ArrayNode.class);
        List<String> keywords = StreamSupport.stream(amazonResponse.get(1).spliterator(), false)
                .map(JsonNode::textValue).collect(Collectors.toList());
        LOGGER.info("Parsed amazon keywords: {}", keywords);

        // count score
        return new ResponseEntity<>(new Score(rank(keywords.size()), keyword), HttpStatus.OK);
    }
}
