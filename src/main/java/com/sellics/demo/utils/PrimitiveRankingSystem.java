package com.sellics.demo.utils;

import javax.validation.constraints.Positive;

/**
 * Primitive ranking util.
 *
 */
public abstract class PrimitiveRankingSystem {
    /**
     * Count keyword rank depending on count.
     *
     * @param count overall keywords count
     * @return primitive rank
     */
    public static int rank(@Positive int count) {
        return count / 10 * 100;
    }
}
