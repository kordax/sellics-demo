package com.sellics.demo.config;

import com.sellics.demo.client.AmazonCompleteClient;
import com.sellics.demo.client.AmazonCompleteClientBlockingImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonCompleteClientConfig {
    @Value("${amazon.api.uri:https://completion.amazon.com/search/complete}")
    String uri;

    @Bean
    public AmazonCompleteClient amazonCompleteClient() {
        return new AmazonCompleteClientBlockingImpl(uri);
    }
}
