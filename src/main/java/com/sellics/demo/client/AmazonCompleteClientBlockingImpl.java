package com.sellics.demo.client;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Amazon Complete API blocking service implementation that uses deprecated {@link RestTemplate} implementation.
 *
 */
public class AmazonCompleteClientBlockingImpl implements AmazonCompleteClient {
    private final static Logger LOGGER = LoggerFactory.getLogger(AmazonCompleteClientBlockingImpl.class);

    private final String uri;
    private final RestTemplate restTemplate = new RestTemplate();


    public AmazonCompleteClientBlockingImpl(String uri) {
        this.uri = uri;
    }

    /**
     * Retrieve keyword score for a keyword.
     * This method is blocking.
     *
     * @param keyword keyword as a String
     * @return keyword score response entity
     */
    @Override
    public ResponseEntity<String> getScoreForKeyword(@NotNull String keyword) {
        LOGGER.info("Getting score for keyword: {}", keyword);

        final String appendedUri = uri + "?search-alias=aps&client=amazon-search-ui&mkt=1&q=" + keyword;
        final ResponseEntity<String> response = restTemplate.getForEntity(appendedUri, String.class);

        LOGGER.info("Received response: {}", response.getBody());

        return response;
    }
}
