package com.sellics.demo.client;


import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;

/**
 * Interface for Amazon Complete API service implementations.
 *
 */
public interface AmazonCompleteClient {
    /**
     * Retrieve keyword score for a keyword.
     *
     * @param keyword keyword as a String
     * @return keyword score
     */
    <T> ResponseEntity<T> getScoreForKeyword(@NotNull String keyword);
}
